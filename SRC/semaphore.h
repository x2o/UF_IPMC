/*
    UF_IPMC/semaphore.h

    Copyright (C) 2020 Aleksei Greshilov
    aleksei.greshilov@cern.ch

    UF_IPMC is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    UF_IPMC is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with UF_IPMC.  If not, see <https://www.gnu.org/licenses/>.
*/

#ifdef __cplusplus
#define SEMAPHORE_EXTERN extern "C"
#else
#define SEMAPHORE_EXTERN extern
#endif

SEMAPHORE_EXTERN void lock (int i2c_bus);
SEMAPHORE_EXTERN void unlock (int i2c_bus);
SEMAPHORE_EXTERN int create_semaphore (int dev_ind);
SEMAPHORE_EXTERN int reset_semaphore (int dev_ind);
