/*
    UF_IPMC/ipmc.h

    Original work Copyright (C) 2007-2008 Gokhan Sozmen
    http://www.coreipm.com

    Modified work Copyright 2020 Aleksei Greshilov
    aleksei.greshilov@cern.ch

    UF_IPMC is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    UF_IPMC is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with UF_IPMC.  If not, see <https://www.gnu.org/licenses/>.
*/

#ifdef __cplusplus
#define IPMC_EXTERN extern "C"
#else
#define IPMC_EXTERN extern
#endif

IPMC_EXTERN void sig_handler(int signo);
IPMC_EXTERN void ipmb_buffers_enable( void );
IPMC_EXTERN void ipmb_buffers_disable( void );
IPMC_EXTERN void module_init( void );
IPMC_EXTERN void module_cold_reset( unsigned char dev_id );
IPMC_EXTERN void module_warm_reset( unsigned char dev_id );
IPMC_EXTERN void module_graceful_reboot( unsigned char dev_id );
IPMC_EXTERN void module_issue_diag_int( unsigned char dev_id );
IPMC_EXTERN void module_quiesce( unsigned char dev_id );
IPMC_EXTERN void module_event_handler( IPMI_PKT *pkt );
IPMC_EXTERN unsigned char module_get_i2c_address( int address_type );
IPMC_EXTERN void module_term_process( unsigned char * );
IPMC_EXTERN void module_led_on( unsigned led_state );
IPMC_EXTERN void module_led_off( unsigned led_state );
IPMC_EXTERN void module_process_response( IPMI_WS *req_ws, unsigned char seq, unsigned char completion_code );
IPMC_EXTERN void module_sensor_init( void );
IPMC_EXTERN void module_rearm_events( void );
IPMC_EXTERN void watchdog_init( void );
IPMC_EXTERN void fru_data_init( void );
IPMC_EXTERN void read_fru_hot_swap( void );
IPMC_EXTERN void read_ipmb_0_status( void );
IPMC_EXTERN void read_hot_swap_handle( void );
IPMC_EXTERN void watchdog_ping( void );
