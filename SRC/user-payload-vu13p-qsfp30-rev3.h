/*
    UF_IPMC/user-payload.h

    Copyright (C) 2020 Aleksei Greshilov
    aleksei.greshilov@cern.ch

    UF_IPMC is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    UF_IPMC is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with UF_IPMC.  If not, see <https://www.gnu.org/licenses/>.
*/
#ifdef __cplusplus
#define USER_PAYLOAD extern "C"
#else
#define USER_PAYLOAD extern
#endif

USER_PAYLOAD void user_module_payload_on( void );
USER_PAYLOAD void user_module_payload_off( void );
USER_PAYLOAD void payload_state_poll( unsigned char *arg );
USER_PAYLOAD int check_power_up( void );
