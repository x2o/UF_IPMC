/*
   UF_IPMC/user-sensor.c

   Copyright (C) 2020 Aleksei Greshilov
   aleksei.greshilov@cern.ch

   UF_IPMC is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   UF_IPMC is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with UF_IPMC.  If not, see <https://www.gnu.org/licenses/>.
   */
#include "string.h"
#include "ipmi.h"
#include "picmg.h"
#include "event.h"
#include "ipmc.h"
#include "i2c.h"
#include "i2c-sensor.h"
#include "event.h"
#include "debug.h"
#include "timer.h"
#include "sensor.h"
#include "logger.h"
#include "user-payload-vu13p-qsfp30-rev3.h"
#include "user-sensor-vu13p-qsfp30-rev3.h"
#include "semaphore.h"
#include <stdio.h>
#include <stdint.h>
#include <stdlib.h>
#include <unistd.h>
#include <fcntl.h>
#include <string.h>
#include <errno.h>
#include <sys/types.h>
#include <sys/mman.h>
#include <sys/stat.h>
#include <linux/limits.h>
#include <algorithm>
#include <map>
#include <cstring>
#include "x2o/octopus.h"

#define qbv_on_off              0x1220000
#define xadc_temp_offset				0x4000400
#define payload_off_alarm				0x1220008

using namespace x2o;

extern unsigned long long int lbolt;
extern unsigned long long int payload_timeout_init;
extern FRU_CACHE fru_inventory_cache[];
extern unsigned char current_sensor_count;
extern SDR_ENTRY sdr_entry_table[];
extern int i2c_fd_snsr[];
extern FULL_SENSOR_RECORD sdr[];
extern SENSOR_DATA sd[];
extern int optics_powered;

unsigned char busid = 1;
i2c_bus octo_bus(busid);

unsigned char temp_max = 0;

std::map<std::string, octopus::monitor_table> monitor;

// map of the qsfp selection bits.
// refdes numbers in comments match schematics
qsfp_select_t qsfp_select [30] =
		{
			{1, 0x24}, // cage 0 refdes 0
			{1, 0x3E}, // cage 1 refdes 1
			{1, 0x3D}, // cage 2 refdes 2
			{1, 0x3B}, // cage 3 refdes 3
			{1, 0x39}, // cage 4 refdes 4
			{1, 0x36}, // cage 5 refdes 5
			{0, 0x29}, // cage 6 refdes 6
			{0, 0x2B}, // cage 7 refdes 7
			{0, 0x30}, // cage 8 refdes 8
			{0, 0x32}, // cage 9 refdes 9
			{0, 0x38}, // cage 10 refdes 10
			{0, 0x3A}, // cage 11 refdes 11
			{0, 0x3C}, // cage 12 refdes 12
			{0, 0x3D}, // cage 13 refdes 13
			{0, 0x3F}, // cage 14 refdes 28
			{1, 0x3F}, // cage 15 refdes 14
			{1, 0x26}, // cage 16 refdes 15
			{1, 0x27}, // cage 17 refdes 16
			{1, 0x3A}, // cage 18 refdes 17
			{1, 0x38}, // cage 19 refdes 18
			{1, 0x35}, // cage 20 refdes 19
			{0, 0x2E}, // cage 21 refdes 20
			{0, 0x2F}, // cage 22 refdes 21
			{0, 0x31}, // cage 23 refdes 22
			{0, 0x33}, // cage 24 refdes 23
			{0, 0x39}, // cage 25 refdes 24
			{0, 0x3B}, // cage 26 refdes 25
			{0, 0x27}, // cage 27 refdes 26
			{0, 0x25}, // cage 28 refdes 27
			{0, 0x24}  // cage 29 refdes 29
		};

/*==============================================================*/
/* Local Function Prototypes					*/
/*==============================================================*/
void pgood_state_poll( unsigned char *arg );
void temp_qsfp30_max_poll( unsigned char *arg );
void temp_DSE0133V2NBC_poll( unsigned char *arg );
void temp_PIM400KZ_poll( unsigned char *arg );
void temp_XADC_poll( unsigned char *arg );
void temp_FPGA_poll( unsigned char *arg );
void temp_LTM4638_poll( unsigned char *arg );
void exp_reg_config_qsfp3( void );

/*==============================================================*/
/* USER SEMAPHORE INITIALIZATION				*/
/*==============================================================*/
void semaphore_initialize(void) {
	if (create_semaphore(1) < 0) {
		logger("ERROR", "Semaphore initialization failed for sensor bus 1");
	} else {
		logger("SUCCESS", "Semaphore initialization complete for sensor bus 1");
	}

	if (create_semaphore(2) < 0) {
		logger("ERROR", "Semaphore initialization failed for sensor bus 2");
	} else {
		logger("SUCCESS", "Semaphore initialization complete for sensor bus 2");
	}

	if (create_semaphore(3) < 0) {
		logger("ERROR", "Semaphore initialization failed for sensor bus 3");
	} else {
		logger("SUCCESS", "Semaphore initialization complete for sensor bus 3");
	}

	if (create_semaphore(4) < 0) {
		logger("ERROR", "Semaphore initialization failed for sensor bus 4");
	} else {
		logger("SUCCESS", "Semaphore initialization complete for sensor bus 4");
	}

	if (create_semaphore(5) < 0) {
		logger("ERROR", "Semaphore initialization failed for sensor bus 5");
	} else {
		logger("SUCCESS", "Semaphore initialization complete for sensor bus 5");
	}
}

void user_sensor_state_poll(void) {
	pgood_state_poll( 0 );
	//temp_qsfp30_max_poll( 0 );
	//temp_DSE0133V2NBC_poll( 0 );
	//temp_PIM400KZ_poll( 0 );
	//temp_XADC_poll( 0 );
	//temp_FPGA_poll( 0 );
	//temp_LTM4638_poll( 0 );
} // end of user_module_init() function

/*==============================================================
 *  * USER MODULES INITIALIZATION
 *   *==============================================================*/
void user_modules_init(void) {
  exp_reg_config_qsfp3(); // qsfp preconfigure
	octo_bus.init(); //i2c bus of Octopus module
}

void exp_reg_config_qsfp3( void ) {
  u8 i2c_ch = 2;

  // configure inputs and outputs
  i2c_write(i2c_fd_snsr[i2c_ch], 0x44,  9, 0x75);
  i2c_write(i2c_fd_snsr[i2c_ch], 0x44, 10, 0x77);
  i2c_write(i2c_fd_snsr[i2c_ch], 0x44, 11, 0x5F);
  i2c_write(i2c_fd_snsr[i2c_ch], 0x44, 12, 0x55);
  i2c_write(i2c_fd_snsr[i2c_ch], 0x44, 13, 0xFF);
  i2c_write(i2c_fd_snsr[i2c_ch], 0x44, 14, 0x55);
  i2c_write(i2c_fd_snsr[i2c_ch], 0x44, 15, 0x75);
  i2c_write(i2c_fd_snsr[i2c_ch], 0x44,  9, 0x5D);
  i2c_write(i2c_fd_snsr[i2c_ch], 0x44, 10, 0xFF);
  i2c_write(i2c_fd_snsr[i2c_ch], 0x44, 11, 0xFD);
  i2c_write(i2c_fd_snsr[i2c_ch], 0x44, 12, 0xFF);
  i2c_write(i2c_fd_snsr[i2c_ch], 0x44, 13, 0xD7);
  i2c_write(i2c_fd_snsr[i2c_ch], 0x44, 14, 0x55);
  i2c_write(i2c_fd_snsr[i2c_ch], 0x44, 15, 0x57);

  // write defaults, all 1s
  i2c_write(i2c_fd_snsr[i2c_ch], 0x44, 0x44, 0xFF);
  i2c_write(i2c_fd_snsr[i2c_ch], 0x44, 0x4c, 0xFF);
  i2c_write(i2c_fd_snsr[i2c_ch], 0x44, 0x54, 0xFF);
  i2c_write(i2c_fd_snsr[i2c_ch], 0x44, 0x5c, 0xFF);
  i2c_write(i2c_fd_snsr[i2c_ch], 0x41, 0x44, 0xFF);
  i2c_write(i2c_fd_snsr[i2c_ch], 0x41, 0x4c, 0xFF);
  i2c_write(i2c_fd_snsr[i2c_ch], 0x41, 0x54, 0xFF);
  i2c_write(i2c_fd_snsr[i2c_ch], 0x41, 0x5c, 0xFF);

  // enable operation
  i2c_write(i2c_fd_snsr[i2c_ch], 0x44, 4, 1);
  i2c_write(i2c_fd_snsr[i2c_ch], 0x41, 4, 1);
}

/*==============================================================
 *  * USER MODULE SENSORS INITIALIZATION
 *   *==============================================================*/
void user_module_sensor_init(void) {
	/*==============================================================*/
	/* 		All Sensors											*/
	/*==============================================================*/
  sd[4].scan_function = read_sensor_pgood;
	sd[5].scan_function = read_sensor_temp_qsfp30_max;
	sd[6].scan_function = read_sensor_temp_DSE0133V2NBC;
	sd[7].scan_function = read_sensor_temp_PIM400KZ;
	sd[8].scan_function = read_sensor_temp_XADC;
	sd[9].scan_function = read_sensor_temp_FPGA;
	sd[10].scan_function = read_sensor_temp_LTM4638;
}

/*==============================================================*/
/* 	 		PGOOD Sensor				                        */
/*==============================================================*/
void read_sensor_pgood(void) {
	//	Sensor Data Record
	u8 sensor_N = 4;
	u8 result = 0;
	u8 bot = 0;
	u8 top = 0;
	u8 qsfp = 0;
	if (check_power_up())
	{
		result = reg_read(devmem_ptr, payload_off_alarm);
		bot = (result>>25)&0x1;
		top = (result>>26)&0x1;
		qsfp = (result>>27)&0x1;

		sd[sensor_N].last_sensor_reading = 1;
		sd[sensor_N].sensor_scanning_enabled = 1;
		sd[sensor_N].event_messages_enabled = 1;
		sd[sensor_N].unavailable = 0;

		if (bot || top || qsfp)
		{
			if (bot)
			{
				logger("POK ALARM", "Payload off ALARM in Bottom FPGA");
			}
			if (top)
			{
				logger("POK ALARM", "Payload off ALARM in Top FPGA");
			}
			if (qsfp)
			{
				logger("POK ALARM", "Payload off ALARM in QSFP module");
			}

			picmg_m6_state(fru_inventory_cache[0].fru_dev_id);
		}
	}
	else
	{
		sd[sensor_N].last_sensor_reading = 0;
		sd[sensor_N].sensor_scanning_enabled = 0;
		sd[sensor_N].event_messages_enabled = 0;
		sd[sensor_N].unavailable = 1;
	}
}

/*==============================================================*/
/* 	 		Temp QSFP Max Sensor				                        */
/*==============================================================*/
void read_sensor_temp_qsfp30_max(void) {
    lock(3);

    uint64_t exp[2] = {0,0};
    uint64_t qsfp_mod = 0;

	exp[0] = 0x44; // expansion register on top layer
	exp[1] = 0x41; // expansion register on bottom layer
	qsfp_mod = 0x50; // QSFP module address

    //	Sensor Data Record
    u8 sensor_N = 5;
	u8 i2c_ch = 2;

    if (check_power_up()) {
        for (int i = 0; i < 30; i++)
        {
          qsfp_select_t qs = qsfp_select[i];

          // reset all outputs to 1
          for (int j = 0; j < 2; j++)
          {
            i2c_write (i2c_fd_snsr[i2c_ch], exp[j], 0x44, 0xff);
            i2c_write (i2c_fd_snsr[i2c_ch], exp[j], 0x4c, 0xff);
            i2c_write (i2c_fd_snsr[i2c_ch], exp[j], 0x54, 0xff);
            i2c_write (i2c_fd_snsr[i2c_ch], exp[j], 0x5c, 0xff);
          }

          // write selection bit
          i2c_write (i2c_fd_snsr[i2c_ch], exp[qs.chip_ind], qs.reg_addr, 0);

          t.t16 = 0;
          int res;
          res = i2c_read (i2c_fd_snsr[i2c_ch], qsfp_mod, 0x16, &t.t8[1]);
          // if the first read fails, the device is not there, skip the rest
          if (res != -1)
          {
            i2c_read (i2c_fd_snsr[i2c_ch], qsfp_mod, 0x17, &t.t8[0]);

            // convert to C according to QSFP-MSA.pdf page 53 line 14
            float temp_f = ((float)t.t16)/256.;

            v.v16 = 0;
            i2c_read (i2c_fd_snsr[i2c_ch], qsfp_mod, 0x1a, &v.v8[1]);
            i2c_read (i2c_fd_snsr[i2c_ch], qsfp_mod, 0x1b, &v.v8[0]);

            // convert to V according to QSFP-MSA.pdf page 53 line 19
            float vv = ((float)v.v16)/10000.;

            if (vv < 3.2)
            {
              printf ("device %d voltage = %1.2f\n", i, vv);
            }

            //	Convert float to byte and get precision
            u8 temp_b = (u8)(temp_f);

            if (temp_b >= temp_max)
            {
                temp_max = temp_b;
            }
          }
        }

				sd[sensor_N].last_sensor_reading = temp_max;
				sd[sensor_N].sensor_scanning_enabled = 1;
				sd[sensor_N].event_messages_enabled = 1;
				sd[sensor_N].unavailable = 0;

				temp_max = 0;

        static int first_time = 1;
        static int up_noncrt_assert = 0;

        if (first_time) {
            first_time = 0;
        } else if (sd[sensor_N].last_sensor_reading >= sdr[sensor_N].upper_non_recoverable_threshold) {
            // Transition to M6 for non-recoverable
						sd[sensor_N].current_state_mask = 0xE0;
            unlock(3);
            picmg_m6_state(fru_inventory_cache[0].fru_dev_id);
          	logger("WARNING","Non-recoverable threshold crossed for QSFP temperature sensor");
            lock(3);
        } else if (up_noncrt_assert == 0 && sd[sensor_N].last_sensor_reading >= sdr[sensor_N].upper_critical_threshold) {
						sd[sensor_N].current_state_mask = 0xD0;
						// Assertion message for shelf manager
          	FRU_TEMPERATURE_EVENT_MSG_REQ msg;
          	msg.command = 0x02;
			      msg.evt_msg_rev = 0x04;
          	msg.sensor_type = 0x01;
          	msg.sensor_number = sensor_N;
          	msg.evt_direction = 0x01;
            msg.evt_data2_qual = 0x01;
            msg.evt_data3_qual = 0x01;
            msg.evt_reason = 0x07;
            msg.temp_reading = sd[sensor_N].last_sensor_reading;
            msg.threshold = sdr[sensor_N].upper_critical_threshold;

            ipmi_send_event_req(( unsigned char * )&msg, sizeof(FRU_TEMPERATURE_EVENT_MSG_REQ), 0);
            up_noncrt_assert = 1;
				} else if (up_noncrt_assert == 1 && sd[sensor_N].last_sensor_reading < sdr[sensor_N].upper_critical_threshold) {
						sd[sensor_N].current_state_mask = 0xC0;
            // Deassertion message for shelf manager
            FRU_TEMPERATURE_EVENT_MSG_REQ msg;
            msg.command = 0x02;
            msg.evt_msg_rev = 0x04;
            msg.sensor_type = 0x01;
          	msg.sensor_number = sensor_N;
            msg.evt_direction = 0x81;
            msg.evt_data2_qual = 0x01;
            msg.evt_data3_qual = 0x01;
            msg.evt_reason = 0x07;
            msg.temp_reading = sd[sensor_N].last_sensor_reading;
            msg.threshold = sdr[sensor_N].upper_critical_threshold;

            ipmi_send_event_req(( unsigned char * )&msg, sizeof(FRU_TEMPERATURE_EVENT_MSG_REQ), 0);
            up_noncrt_assert = 0;
        } else if (up_noncrt_assert == 0 && sd[sensor_N].last_sensor_reading >= sdr[sensor_N].upper_non_critical_threshold) {
						sd[sensor_N].current_state_mask = 0xC8;
            // Assertion message for shelf manager
            FRU_TEMPERATURE_EVENT_MSG_REQ msg;
            msg.command = 0x02;
			      msg.evt_msg_rev = 0x04;
            msg.sensor_type = 0x01;
            msg.sensor_number = sensor_N;
            msg.evt_direction = 0x01;
            msg.evt_data2_qual = 0x01;
            msg.evt_data3_qual = 0x01;
          	msg.evt_reason = 0x07;
            msg.temp_reading = sd[sensor_N].last_sensor_reading;
            msg.threshold = sdr[sensor_N].upper_non_critical_threshold;

            ipmi_send_event_req(( unsigned char * )&msg, sizeof(FRU_TEMPERATURE_EVENT_MSG_REQ), 0);
            up_noncrt_assert = 2;
        } else if (up_noncrt_assert == 2 && sd[sensor_N].last_sensor_reading < sdr[sensor_N].upper_non_critical_threshold) {
						sd[sensor_N].current_state_mask = 0xC0;
          	// Deassertion message for shelf manager
            FRU_TEMPERATURE_EVENT_MSG_REQ msg;
            msg.command = 0x02;
          	msg.evt_msg_rev = 0x04;
            msg.sensor_type = 0x01;
            msg.sensor_number = sensor_N;
            msg.evt_direction = 0x81;
            msg.evt_data2_qual = 0x01;
            msg.evt_data3_qual = 0x01;
            msg.evt_reason = 0x07;
            msg.temp_reading = sd[sensor_N].last_sensor_reading;
            msg.threshold = sdr[sensor_N].upper_non_critical_threshold;

            ipmi_send_event_req(( unsigned char * )&msg, sizeof(FRU_TEMPERATURE_EVENT_MSG_REQ), 0);
            up_noncrt_assert = 0;
        }
    } else {
				sd[sensor_N].last_sensor_reading = 0;
				sd[sensor_N].sensor_scanning_enabled = 0;
				sd[sensor_N].event_messages_enabled = 0;
				sd[sensor_N].unavailable = 1;
				sd[sensor_N].current_state_mask = 0;
    }
    unlock(3);
}

/*==============================================================*/
/* 	 		Temp DSE0133V2NBC Sensor				                        */
/*==============================================================*/
void read_sensor_temp_DSE0133V2NBC(void) {
    lock(5);

    //	Sensor Data Record
    u8 sensor_N = 6;
	u8 i2c_ch = 4;
	u8 i2c_addr = 0x36;
	u8 pmbus_cmd = 0x8D;
	u16 result = 0;
    linear11_val_t t;
    double temp = 0.0;

	pmbus_two_bytes_read(i2c_fd_snsr[i2c_ch], i2c_addr, pmbus_cmd, &result);

    t.raw = result;

    if (t.linear.mantissa < 0)
    {
      short int mantissa = -t.linear.mantissa;
      double tmp = (double) (1 << mantissa);
      temp = t.linear.base * (1/tmp);
    }
    else
    {
      temp = t.linear.base * (double) (1 << t.linear.mantissa);
    }

		u8 temp_b = (u8)(temp);

		sd[sensor_N].last_sensor_reading = temp_b;
		sd[sensor_N].sensor_scanning_enabled = 1;
		sd[sensor_N].event_messages_enabled = 1;
		sd[sensor_N].unavailable = 0;

		static int first_time = 1;
		static int up_noncrt_assert = 0;

		if (first_time) {
						first_time = 0;
		} else if (sd[sensor_N].last_sensor_reading >= sdr[sensor_N].upper_non_recoverable_threshold) {
				// Transition to M6 for non-recoverable
				sd[sensor_N].current_state_mask = 0xE0;
				unlock(5);
				picmg_m6_state(fru_inventory_cache[0].fru_dev_id);
				logger("WARNING","Non-recoverable threshold crossed for DSE0133V2NBC temperature sensor");
				lock(5);
		} else if (up_noncrt_assert == 0 && sd[sensor_N].last_sensor_reading >= sdr[sensor_N].upper_critical_threshold) {
				sd[sensor_N].current_state_mask = 0xD0;
				// Assertion message for shelf manager
				FRU_TEMPERATURE_EVENT_MSG_REQ msg;
				msg.command = 0x02;
				msg.evt_msg_rev = 0x04;
				msg.sensor_type = 0x01;
				msg.sensor_number = sensor_N;
				msg.evt_direction = 0x01;
				msg.evt_data2_qual = 0x01;
				msg.evt_data3_qual = 0x01;
				msg.evt_reason = 0x07;
				msg.temp_reading = sd[sensor_N].last_sensor_reading;
				msg.threshold = sdr[sensor_N].upper_critical_threshold;

				ipmi_send_event_req(( unsigned char * )&msg, sizeof(FRU_TEMPERATURE_EVENT_MSG_REQ), 0);
				up_noncrt_assert = 1;
		} else if (up_noncrt_assert == 1 && sd[sensor_N].last_sensor_reading < sdr[sensor_N].upper_critical_threshold) {
				sd[sensor_N].current_state_mask = 0xC0;
				// Deassertion message for shelf manager
				FRU_TEMPERATURE_EVENT_MSG_REQ msg;
				msg.command = 0x02;
				msg.evt_msg_rev = 0x04;
				msg.sensor_type = 0x01;
				msg.sensor_number = sensor_N;
				msg.evt_direction = 0x81;
				msg.evt_data2_qual = 0x01;
				msg.evt_data3_qual = 0x01;
				msg.evt_reason = 0x07;
				msg.temp_reading = sd[sensor_N].last_sensor_reading;
				msg.threshold = sdr[sensor_N].upper_critical_threshold;

				ipmi_send_event_req(( unsigned char * )&msg, sizeof(FRU_TEMPERATURE_EVENT_MSG_REQ), 0);
				up_noncrt_assert = 0;
		} else if (up_noncrt_assert == 0 && sd[sensor_N].last_sensor_reading >= sdr[sensor_N].upper_non_critical_threshold) {
				sd[sensor_N].current_state_mask = 0xC8;
				// Assertion message for shelf manager
				FRU_TEMPERATURE_EVENT_MSG_REQ msg;
				msg.command = 0x02;
				msg.evt_msg_rev = 0x04;
				msg.sensor_type = 0x01;
				msg.sensor_number = sensor_N;
				msg.evt_direction = 0x01;
				msg.evt_data2_qual = 0x01;
				msg.evt_data3_qual = 0x01;
				msg.evt_reason = 0x07;
				msg.temp_reading = sd[sensor_N].last_sensor_reading;
				msg.threshold = sdr[sensor_N].upper_non_critical_threshold;

				ipmi_send_event_req(( unsigned char * )&msg, sizeof(FRU_TEMPERATURE_EVENT_MSG_REQ), 0);
				up_noncrt_assert = 2;
		} else if (up_noncrt_assert == 2 && sd[sensor_N].last_sensor_reading < sdr[sensor_N].upper_non_critical_threshold) {
				sd[sensor_N].current_state_mask = 0xC0;
				// Deassertion message for shelf manager
				FRU_TEMPERATURE_EVENT_MSG_REQ msg;
				msg.command = 0x02;
				msg.evt_msg_rev = 0x04;
				msg.sensor_type = 0x01;
				msg.sensor_number = sensor_N;
				msg.evt_direction = 0x81;
				msg.evt_data2_qual = 0x01;
				msg.evt_data3_qual = 0x01;
				msg.evt_reason = 0x07;
				msg.temp_reading = sd[sensor_N].last_sensor_reading;
				msg.threshold = sdr[sensor_N].upper_non_critical_threshold;

				ipmi_send_event_req(( unsigned char * )&msg, sizeof(FRU_TEMPERATURE_EVENT_MSG_REQ), 0);
				up_noncrt_assert = 0;
		}

    unlock(5);
}

/*==============================================================*/
/* 	 		Temp PIM400KZ Sensor				                        */
/*==============================================================*/
void read_sensor_temp_PIM400KZ(void) {
    lock(5);

    //	Sensor Data Record
    	u8 sensor_N = 7;
		u8 i2c_ch = 4;
		u8 i2c_addr = 0x2f;
		u8 pmbus_cmd = 0x28;
		u8 result = 0;

		i2c_read(i2c_fd_snsr[i2c_ch], i2c_addr, pmbus_cmd, &result);
		float temp_f = ((float) result)*1.961 - 50;
		u8 temp_b = (u8)(temp_f);

		sd[sensor_N].last_sensor_reading = temp_b;
		sd[sensor_N].sensor_scanning_enabled = 1;
		sd[sensor_N].event_messages_enabled = 1;
		sd[sensor_N].unavailable = 0;

		static int first_time = 1;
		static int up_noncrt_assert = 0;

		if (first_time) {
						first_time = 0;
		} else if (sd[sensor_N].last_sensor_reading >= sdr[sensor_N].upper_non_recoverable_threshold) {
				// Transition to M6 for non-recoverable
				sd[sensor_N].current_state_mask = 0xE0;
				unlock(5);
				picmg_m6_state(fru_inventory_cache[0].fru_dev_id);
				logger("WARNING","Non-recoverable threshold crossed for PIM400KZ temperature sensor");
				lock(5);
		} else if (up_noncrt_assert == 0 && sd[sensor_N].last_sensor_reading >= sdr[sensor_N].upper_critical_threshold) {
				sd[sensor_N].current_state_mask = 0xD0;
				// Assertion message for shelf manager
				FRU_TEMPERATURE_EVENT_MSG_REQ msg;
				msg.command = 0x02;
				msg.evt_msg_rev = 0x04;
				msg.sensor_type = 0x01;
				msg.sensor_number = sensor_N;
				msg.evt_direction = 0x01;
				msg.evt_data2_qual = 0x01;
				msg.evt_data3_qual = 0x01;
				msg.evt_reason = 0x07;
				msg.temp_reading = sd[sensor_N].last_sensor_reading;
				msg.threshold = sdr[sensor_N].upper_critical_threshold;

				ipmi_send_event_req(( unsigned char * )&msg, sizeof(FRU_TEMPERATURE_EVENT_MSG_REQ), 0);
				up_noncrt_assert = 1;
		} else if (up_noncrt_assert == 1 && sd[sensor_N].last_sensor_reading < sdr[sensor_N].upper_critical_threshold) {
				sd[sensor_N].current_state_mask = 0xC0;
				// Deassertion message for shelf manager
				FRU_TEMPERATURE_EVENT_MSG_REQ msg;
				msg.command = 0x02;
				msg.evt_msg_rev = 0x04;
				msg.sensor_type = 0x01;
				msg.sensor_number = sensor_N;
				msg.evt_direction = 0x81;
				msg.evt_data2_qual = 0x01;
				msg.evt_data3_qual = 0x01;
				msg.evt_reason = 0x07;
				msg.temp_reading = sd[sensor_N].last_sensor_reading;
				msg.threshold = sdr[sensor_N].upper_critical_threshold;

				ipmi_send_event_req(( unsigned char * )&msg, sizeof(FRU_TEMPERATURE_EVENT_MSG_REQ), 0);
				up_noncrt_assert = 0;
		} else if (up_noncrt_assert == 0 && sd[sensor_N].last_sensor_reading >= sdr[sensor_N].upper_non_critical_threshold) {
				sd[sensor_N].current_state_mask = 0xC8;
				// Assertion message for shelf manager
				FRU_TEMPERATURE_EVENT_MSG_REQ msg;
				msg.command = 0x02;
				msg.evt_msg_rev = 0x04;
				msg.sensor_type = 0x01;
				msg.sensor_number = sensor_N;
				msg.evt_direction = 0x01;
				msg.evt_data2_qual = 0x01;
				msg.evt_data3_qual = 0x01;
				msg.evt_reason = 0x07;
				msg.temp_reading = sd[sensor_N].last_sensor_reading;
				msg.threshold = sdr[sensor_N].upper_non_critical_threshold;

				ipmi_send_event_req(( unsigned char * )&msg, sizeof(FRU_TEMPERATURE_EVENT_MSG_REQ), 0);
				up_noncrt_assert = 2;
		} else if (up_noncrt_assert == 2 && sd[sensor_N].last_sensor_reading < sdr[sensor_N].upper_non_critical_threshold) {
				sd[sensor_N].current_state_mask = 0xC0;
				// Deassertion message for shelf manager
				FRU_TEMPERATURE_EVENT_MSG_REQ msg;
				msg.command = 0x02;
				msg.evt_msg_rev = 0x04;
				msg.sensor_type = 0x01;
				msg.sensor_number = sensor_N;
				msg.evt_direction = 0x81;
				msg.evt_data2_qual = 0x01;
				msg.evt_data3_qual = 0x01;
				msg.evt_reason = 0x07;
				msg.temp_reading = sd[sensor_N].last_sensor_reading;
				msg.threshold = sdr[sensor_N].upper_non_critical_threshold;

				ipmi_send_event_req(( unsigned char * )&msg, sizeof(FRU_TEMPERATURE_EVENT_MSG_REQ), 0);
				up_noncrt_assert = 0;
		}

    unlock(5);
}

/*==============================================================*/
/* 	 		Temp XADC Sensor				                        */
/*==============================================================*/
void read_sensor_temp_XADC(void) {
    //	Sensor Data Record
    u8 sensor_N = 8;
	u32 result = 0;
    u16 temp_d = 0;
    u32 xadc_reg_rw;

	result = reg_read(devmem_ptr, xadc_temp_offset);
    temp_d = (result >> 6)&0x3ff;
	float temp_f = (((float) temp_d)*503.975)/1024. - 273.15;
	u8 temp_b = (u8)(temp_f);

	sd[sensor_N].last_sensor_reading = temp_b;
	sd[sensor_N].sensor_scanning_enabled = 1;
	sd[sensor_N].event_messages_enabled = 1;
	sd[sensor_N].unavailable = 0;

	static int first_time = 1;
	static int up_noncrt_assert = 0;

		if (first_time) {
						first_time = 0;
		} else if (sd[sensor_N].last_sensor_reading >= sdr[sensor_N].upper_non_recoverable_threshold) {
				// Transition to M6 for non-recoverable
				sd[sensor_N].current_state_mask = 0xE0;
				picmg_m6_state(fru_inventory_cache[0].fru_dev_id);
				logger("WARNING","Non-recoverable threshold crossed for XADC temperature sensor");
		} else if (up_noncrt_assert == 0 && sd[sensor_N].last_sensor_reading >= sdr[sensor_N].upper_critical_threshold) {
				sd[sensor_N].current_state_mask = 0xD0;
				// Assertion message for shelf manager
				FRU_TEMPERATURE_EVENT_MSG_REQ msg;
				msg.command = 0x02;
				msg.evt_msg_rev = 0x04;
				msg.sensor_type = 0x01;
				msg.sensor_number = sensor_N;
				msg.evt_direction = 0x01;
				msg.evt_data2_qual = 0x01;
				msg.evt_data3_qual = 0x01;
				msg.evt_reason = 0x07;
				msg.temp_reading = sd[sensor_N].last_sensor_reading;
				msg.threshold = sdr[sensor_N].upper_critical_threshold;

				ipmi_send_event_req(( unsigned char * )&msg, sizeof(FRU_TEMPERATURE_EVENT_MSG_REQ), 0);
				up_noncrt_assert = 1;
		} else if (up_noncrt_assert == 1 && sd[sensor_N].last_sensor_reading < sdr[sensor_N].upper_critical_threshold) {
				sd[sensor_N].current_state_mask = 0xC0;
				// Deassertion message for shelf manager
				FRU_TEMPERATURE_EVENT_MSG_REQ msg;
				msg.command = 0x02;
				msg.evt_msg_rev = 0x04;
				msg.sensor_type = 0x01;
				msg.sensor_number = sensor_N;
				msg.evt_direction = 0x81;
				msg.evt_data2_qual = 0x01;
				msg.evt_data3_qual = 0x01;
				msg.evt_reason = 0x07;
				msg.temp_reading = sd[sensor_N].last_sensor_reading;
				msg.threshold = sdr[sensor_N].upper_critical_threshold;

				ipmi_send_event_req(( unsigned char * )&msg, sizeof(FRU_TEMPERATURE_EVENT_MSG_REQ), 0);
				up_noncrt_assert = 0;
		} else if (up_noncrt_assert == 0 && sd[sensor_N].last_sensor_reading >= sdr[sensor_N].upper_non_critical_threshold) {
				sd[sensor_N].current_state_mask = 0xC8;
				// Assertion message for shelf manager
				FRU_TEMPERATURE_EVENT_MSG_REQ msg;
				msg.command = 0x02;
				msg.evt_msg_rev = 0x04;
				msg.sensor_type = 0x01;
				msg.sensor_number = sensor_N;
				msg.evt_direction = 0x01;
				msg.evt_data2_qual = 0x01;
				msg.evt_data3_qual = 0x01;
				msg.evt_reason = 0x07;
				msg.temp_reading = sd[sensor_N].last_sensor_reading;
				msg.threshold = sdr[sensor_N].upper_non_critical_threshold;

				ipmi_send_event_req(( unsigned char * )&msg, sizeof(FRU_TEMPERATURE_EVENT_MSG_REQ), 0);
				up_noncrt_assert = 2;
		} else if (up_noncrt_assert == 2 && sd[sensor_N].last_sensor_reading < sdr[sensor_N].upper_non_critical_threshold) {
				sd[sensor_N].current_state_mask = 0xC0;
				// Deassertion message for shelf manager
				FRU_TEMPERATURE_EVENT_MSG_REQ msg;
				msg.command = 0x02;
				msg.evt_msg_rev = 0x04;
				msg.sensor_type = 0x01;
				msg.sensor_number = sensor_N;
				msg.evt_direction = 0x81;
				msg.evt_data2_qual = 0x01;
				msg.evt_data3_qual = 0x01;
				msg.evt_reason = 0x07;
				msg.temp_reading = sd[sensor_N].last_sensor_reading;
				msg.threshold = sdr[sensor_N].upper_non_critical_threshold;

				ipmi_send_event_req(( unsigned char * )&msg, sizeof(FRU_TEMPERATURE_EVENT_MSG_REQ), 0);
				up_noncrt_assert = 0;
		}
}

/*==============================================================*/
/* 	 		Temp FPGA Sensor				                        */
/*==============================================================*/
void read_sensor_temp_FPGA(void) {
    lock(1);

    //	Sensor Data Record
    u8 sensor_N = 9;

    if (check_power_up()) {
				std::string config = "/etc/x2o/octopus.toml";
				octopus octopus(&octo_bus);
				octopus.configure(false, config);
				monitor = octopus.monitor(0);
				float temp_f = monitor["VIRTEXUPLUS"].t1_remote;

				//	Convert float to byte and get precision
				u8 temp_b = (u8)(temp_f);

				sd[sensor_N].last_sensor_reading = temp_b;
				sd[sensor_N].sensor_scanning_enabled = 1;
				sd[sensor_N].event_messages_enabled = 1;
				sd[sensor_N].unavailable = 0;

        static int first_time = 1;
        static int up_noncrt_assert = 0;

        if (first_time) {
            first_time = 0;
        } else if (sd[sensor_N].last_sensor_reading >= sdr[sensor_N].upper_non_recoverable_threshold) {
            // Transition to M6 for non-recoverable
						sd[sensor_N].current_state_mask = 0xE0;
            unlock(1);
            picmg_m6_state(fru_inventory_cache[0].fru_dev_id);
          	logger("WARNING","Non-recoverable threshold crossed for QSFP temperature sensor");
            lock(1);
        } else if (up_noncrt_assert == 0 && sd[sensor_N].last_sensor_reading >= sdr[sensor_N].upper_critical_threshold) {
						sd[sensor_N].current_state_mask = 0xD0;
						// Assertion message for shelf manager
          	FRU_TEMPERATURE_EVENT_MSG_REQ msg;
          	msg.command = 0x02;
			      msg.evt_msg_rev = 0x04;
          	msg.sensor_type = 0x01;
          	msg.sensor_number = sensor_N;
          	msg.evt_direction = 0x01;
            msg.evt_data2_qual = 0x01;
            msg.evt_data3_qual = 0x01;
            msg.evt_reason = 0x07;
            msg.temp_reading = sd[sensor_N].last_sensor_reading;
            msg.threshold = sdr[sensor_N].upper_critical_threshold;

            ipmi_send_event_req(( unsigned char * )&msg, sizeof(FRU_TEMPERATURE_EVENT_MSG_REQ), 0);
            up_noncrt_assert = 1;
				} else if (up_noncrt_assert == 1 && sd[sensor_N].last_sensor_reading < sdr[sensor_N].upper_critical_threshold) {
						sd[sensor_N].current_state_mask = 0xC0;
            // Deassertion message for shelf manager
            FRU_TEMPERATURE_EVENT_MSG_REQ msg;
            msg.command = 0x02;
            msg.evt_msg_rev = 0x04;
            msg.sensor_type = 0x01;
          	msg.sensor_number = sensor_N;
            msg.evt_direction = 0x81;
            msg.evt_data2_qual = 0x01;
            msg.evt_data3_qual = 0x01;
            msg.evt_reason = 0x07;
            msg.temp_reading = sd[sensor_N].last_sensor_reading;
            msg.threshold = sdr[sensor_N].upper_critical_threshold;

            ipmi_send_event_req(( unsigned char * )&msg, sizeof(FRU_TEMPERATURE_EVENT_MSG_REQ), 0);
            up_noncrt_assert = 0;
        } else if (up_noncrt_assert == 0 && sd[sensor_N].last_sensor_reading >= sdr[sensor_N].upper_non_critical_threshold) {
						sd[sensor_N].current_state_mask = 0xC8;
            // Assertion message for shelf manager
            FRU_TEMPERATURE_EVENT_MSG_REQ msg;
            msg.command = 0x02;
			      msg.evt_msg_rev = 0x04;
            msg.sensor_type = 0x01;
            msg.sensor_number = sensor_N;
            msg.evt_direction = 0x01;
            msg.evt_data2_qual = 0x01;
            msg.evt_data3_qual = 0x01;
          	msg.evt_reason = 0x07;
            msg.temp_reading = sd[sensor_N].last_sensor_reading;
            msg.threshold = sdr[sensor_N].upper_non_critical_threshold;

            ipmi_send_event_req(( unsigned char * )&msg, sizeof(FRU_TEMPERATURE_EVENT_MSG_REQ), 0);
            up_noncrt_assert = 2;
        } else if (up_noncrt_assert == 2 && sd[sensor_N].last_sensor_reading < sdr[sensor_N].upper_non_critical_threshold) {
						sd[sensor_N].current_state_mask = 0xC0;
          	// Deassertion message for shelf manager
            FRU_TEMPERATURE_EVENT_MSG_REQ msg;
            msg.command = 0x02;
          	msg.evt_msg_rev = 0x04;
            msg.sensor_type = 0x01;
            msg.sensor_number = sensor_N;
            msg.evt_direction = 0x81;
            msg.evt_data2_qual = 0x01;
            msg.evt_data3_qual = 0x01;
            msg.evt_reason = 0x07;
            msg.temp_reading = sd[sensor_N].last_sensor_reading;
            msg.threshold = sdr[sensor_N].upper_non_critical_threshold;

            ipmi_send_event_req(( unsigned char * )&msg, sizeof(FRU_TEMPERATURE_EVENT_MSG_REQ), 0);
            up_noncrt_assert = 0;
        }
    } else {
				sd[sensor_N].last_sensor_reading = 0;
				sd[sensor_N].sensor_scanning_enabled = 0;
				sd[sensor_N].event_messages_enabled = 0;
				sd[sensor_N].unavailable = 1;
				sd[sensor_N].current_state_mask = 0;
    }
    unlock(1);
}

/*==============================================================*/
/* 	 		Temp LTM4638-0V9-N Sensor				                        */
/*==============================================================*/
void read_sensor_temp_LTM4638(void) {
    lock(1);

    //	Sensor Data Record
    u8 sensor_N = 10;

    if (check_power_up()) {
				std::string config = "/etc/x2o/octopus.toml";
				octopus octopus(&octo_bus);
				octopus.configure(false, config);

				std::vector<float> v {monitor["0V9_MGTAVCC_VUP_N"].t1_remote,
															monitor["0V9_MGTAVCC_VUP_N"].t2_remote,
															monitor["0V9_MGTAVCC_VUP_S"].t1_remote,
															monitor["0V9_MGTAVCC_VUP_S"].t2_remote,
															monitor["1V2_MGTAVTT_VUP_N"].t1_remote,
															monitor["1V2_MGTAVTT_VUP_N"].t2_remote,
															monitor["1V2_MGTAVTT_VUP_S"].t1_remote,
															monitor["1V2_MGTAVTT_VUP_S"].t2_remote};

				std::vector<float>::iterator temp_f_max;
				temp_f_max = std::max_element(v.begin(), v.end());

				//	Convert float to byte and get precision
				u8 temp_b_max = (u8)(*temp_f_max);

				sd[sensor_N].last_sensor_reading = temp_b_max;
				sd[sensor_N].sensor_scanning_enabled = 1;
				sd[sensor_N].event_messages_enabled = 1;
				sd[sensor_N].unavailable = 0;

        static int first_time = 1;
        static int up_noncrt_assert = 0;

        if (first_time) {
            first_time = 0;
        } else if (sd[sensor_N].last_sensor_reading >= sdr[sensor_N].upper_non_recoverable_threshold) {
            // Transition to M6 for non-recoverable
						sd[sensor_N].current_state_mask = 0xE0;
            unlock(1);
            picmg_m6_state(fru_inventory_cache[0].fru_dev_id);
          	logger("WARNING","Non-recoverable threshold crossed for QSFP temperature sensor");
            lock(1);
        } else if (up_noncrt_assert == 0 && sd[sensor_N].last_sensor_reading >= sdr[sensor_N].upper_critical_threshold) {
						sd[sensor_N].current_state_mask = 0xD0;
						// Assertion message for shelf manager
          	FRU_TEMPERATURE_EVENT_MSG_REQ msg;
          	msg.command = 0x02;
			      msg.evt_msg_rev = 0x04;
          	msg.sensor_type = 0x01;
          	msg.sensor_number = sensor_N;
          	msg.evt_direction = 0x01;
            msg.evt_data2_qual = 0x01;
            msg.evt_data3_qual = 0x01;
            msg.evt_reason = 0x07;
            msg.temp_reading = sd[sensor_N].last_sensor_reading;
            msg.threshold = sdr[sensor_N].upper_critical_threshold;

            ipmi_send_event_req(( unsigned char * )&msg, sizeof(FRU_TEMPERATURE_EVENT_MSG_REQ), 0);
            up_noncrt_assert = 1;
				} else if (up_noncrt_assert == 1 && sd[sensor_N].last_sensor_reading < sdr[sensor_N].upper_critical_threshold) {
						sd[sensor_N].current_state_mask = 0xC0;
            // Deassertion message for shelf manager
            FRU_TEMPERATURE_EVENT_MSG_REQ msg;
            msg.command = 0x02;
            msg.evt_msg_rev = 0x04;
            msg.sensor_type = 0x01;
          	msg.sensor_number = sensor_N;
            msg.evt_direction = 0x81;
            msg.evt_data2_qual = 0x01;
            msg.evt_data3_qual = 0x01;
            msg.evt_reason = 0x07;
            msg.temp_reading = sd[sensor_N].last_sensor_reading;
            msg.threshold = sdr[sensor_N].upper_critical_threshold;

            ipmi_send_event_req(( unsigned char * )&msg, sizeof(FRU_TEMPERATURE_EVENT_MSG_REQ), 0);
            up_noncrt_assert = 0;
        } else if (up_noncrt_assert == 0 && sd[sensor_N].last_sensor_reading >= sdr[sensor_N].upper_non_critical_threshold) {
						sd[sensor_N].current_state_mask = 0xC8;
            // Assertion message for shelf manager
            FRU_TEMPERATURE_EVENT_MSG_REQ msg;
            msg.command = 0x02;
			      msg.evt_msg_rev = 0x04;
            msg.sensor_type = 0x01;
            msg.sensor_number = sensor_N;
            msg.evt_direction = 0x01;
            msg.evt_data2_qual = 0x01;
            msg.evt_data3_qual = 0x01;
          	msg.evt_reason = 0x07;
            msg.temp_reading = sd[sensor_N].last_sensor_reading;
            msg.threshold = sdr[sensor_N].upper_non_critical_threshold;

            ipmi_send_event_req(( unsigned char * )&msg, sizeof(FRU_TEMPERATURE_EVENT_MSG_REQ), 0);
            up_noncrt_assert = 2;
        } else if (up_noncrt_assert == 2 && sd[sensor_N].last_sensor_reading < sdr[sensor_N].upper_non_critical_threshold) {
						sd[sensor_N].current_state_mask = 0xC0;
          	// Deassertion message for shelf manager
            FRU_TEMPERATURE_EVENT_MSG_REQ msg;
            msg.command = 0x02;
          	msg.evt_msg_rev = 0x04;
            msg.sensor_type = 0x01;
            msg.sensor_number = sensor_N;
            msg.evt_direction = 0x81;
            msg.evt_data2_qual = 0x01;
            msg.evt_data3_qual = 0x01;
            msg.evt_reason = 0x07;
            msg.temp_reading = sd[sensor_N].last_sensor_reading;
            msg.threshold = sdr[sensor_N].upper_non_critical_threshold;

            ipmi_send_event_req(( unsigned char * )&msg, sizeof(FRU_TEMPERATURE_EVENT_MSG_REQ), 0);
            up_noncrt_assert = 0;
        }
    } else {
				sd[sensor_N].last_sensor_reading = 0;
				sd[sensor_N].sensor_scanning_enabled = 0;
				sd[sensor_N].event_messages_enabled = 0;
				sd[sensor_N].unavailable = 1;
				sd[sensor_N].current_state_mask = 0;
    }
    unlock(1);
}

void pgood_state_poll( unsigned char *arg ) {
        unsigned char pgood_timer_handle;

        read_sensor_pgood();

        // Re-start the timer
        timer_add_callout_queue( (void *)&pgood_timer_handle,
                        0.1*SEC, pgood_state_poll, 0 ); /* 0.1 sec timeout */
}

void temp_qsfp30_max_poll( unsigned char *arg ) {
        unsigned char temp_qsfp30_max_timer_handle;

        read_sensor_temp_qsfp30_max();

        // Re-start the timer
        timer_add_callout_queue( (void *)&temp_qsfp30_max_timer_handle,
                        5*SEC, temp_qsfp30_max_poll, 0 ); /* 5 sec timeout */
}

void temp_DSE0133V2NBC_poll( unsigned char *arg ) {
        unsigned char temp_DSE0133V2NBC_timer_handle;

        read_sensor_temp_DSE0133V2NBC();

        // Re-start the timer
        timer_add_callout_queue( (void *)&temp_DSE0133V2NBC_timer_handle,
                        5*SEC, temp_DSE0133V2NBC_poll, 0 ); /* 5 sec timeout */
}

void temp_PIM400KZ_poll( unsigned char *arg ) {
        unsigned char temp_PIM400KZ_timer_handle;

        read_sensor_temp_DSE0133V2NBC();

        // Re-start the timer
        timer_add_callout_queue( (void *)&temp_PIM400KZ_timer_handle,
                        5*SEC, temp_PIM400KZ_poll, 0 ); /* 5 sec timeout */
}

void temp_XADC_poll( unsigned char *arg ) {
        unsigned char temp_XADC_timer_handle;

        read_sensor_temp_XADC();

        // Re-start the timer
        timer_add_callout_queue( (void *)&temp_XADC_timer_handle,
                        5*SEC, temp_XADC_poll, 0 ); /* 5 sec timeout */
}

void temp_FPGA_poll( unsigned char *arg ) {
        unsigned char temp_FPGA_timer_handle;

        read_sensor_temp_FPGA();

        // Re-start the timer
        timer_add_callout_queue( (void *)&temp_FPGA_timer_handle,
                        5*SEC, temp_FPGA_poll, 0 ); /* 5 sec timeout */
}

void temp_LTM4638_poll( unsigned char *arg ) {
        unsigned char temp_LTM4638_timer_handle;

        read_sensor_temp_LTM4638();

        // Re-start the timer
        timer_add_callout_queue( (void *)&temp_LTM4638_timer_handle,
                        5*SEC, temp_LTM4638_poll, 0 ); /* 5 sec timeout */
}
