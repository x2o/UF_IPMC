/*
    UF_IPMC/i2c-sensor.h

    Copyright (C) 2020 Aleksei Greshilov
    aleksei.greshilov@cern.ch

    UF_IPMC is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    UF_IPMC is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with UF_IPMC.  If not, see <https://www.gnu.org/licenses/>.
*/

#ifdef __cplusplus
#define I2C_SENSOR_EXTERN extern "C"
#else
#define I2C_SENSOR_EXTERN extern
#endif

typedef unsigned char u8;
typedef unsigned short int u16;

I2C_SENSOR_EXTERN int i2c_write(int i2c_fd_snsr, u8 slave_addr, u8 reg, u8 data);
I2C_SENSOR_EXTERN int i2c_read(int i2c_fd_snsr, u8 slave_addr, u8 reg, u8 *result);
I2C_SENSOR_EXTERN int i2c_write_qsfp30(int i2c_fd_bus, u8 i2c_chip_addr, u8 data[3]);
//I2C_SENSOR_EXTERN int i2c_read_qsfp30(int i2c_fd_bus, u8 i2c_chip_addr, u8 *result);
I2C_SENSOR_EXTERN int pmbus_byte_write(int i2c_fd_snsr, u8 slave_addr, u8 reg, u8 data);
I2C_SENSOR_EXTERN int pmbus_two_bytes_read(int i2c_fd_snsr, u8 slave_addr, u8 reg, u16 *result);
I2C_SENSOR_EXTERN void i2c_sensor_initialize(void);
I2C_SENSOR_EXTERN void i2c_sensor_deinitialize(void);
